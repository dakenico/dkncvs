﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyWebApplication1.Models
{
    public class Friends
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string Department { get; set; }
    }
}